import { Component, OnInit } from '@angular/core';
import {Question} from '../../modules/question';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit {
  questions: Question[];
  constructor(private dataService: DataService ) {
    this.questions = this.dataService.getQuestions();
  }

  ngOnInit() {
  }

  addQuestion(question:Question){
    this.dataService.addQuestion(question);
  }
}
